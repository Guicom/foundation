# Create a Drupal project with Phing

## 1. Initialize your Drupal project

This will create the directory for your project.

```
git clone https://github.com/drupal-composer/drupal-project.git [project_name]
cd [project_name]
rm -rf .git
```

## 2. Create docker-compose.yml

Minimal example:

```
version: '2'
services:
  # web with xdebug - actency images
  web:
    # @see https://github.com/Actency/docker-apache-php
    image: actency/docker-apache-php:7.2
    ports:
      - "80:80"
      #- "9001:9000"
    environment:
      - SERVERNAME=[project_name].loc
      - SERVERALIAS=[project_name].loc
      - DOCUMENTROOT=web
      - DRUSH_VERSION=9
    volumes:
      - /home/docker/projets/[project_name]/:/var/www/html
      - /home/docker/:/var/www/.ssh/
    links:
      - database:mysql
    tty: true
    shm_size: 2G
    extra_hosts:
      - "[project_name].loc:127.0.0.1"
      - "docker_host:10.10.10.254" # YOUR IP HERE
    hostname: "[project_name].loc"

  # database container - actency images
  database:
    # @see https://github.com/Actency/docker-mysql
    image: actency/docker-mysql:5.7
    ports:
      - "3306:3306"
    environment:
      - MYSQL_ROOT_PASSWORD=mysqlroot
      - MYSQL_DATABASE=[project_name]
      - MYSQL_USER=mysqlusr
      - MYSQL_PASSWORD=mysqlpwd
```

## 3. Start the containers

```
docker-compose up -d
```

## 4. Log into web container

```
go [project_name]_web_1 bash
cd html
```

## 5. Configure Composer

```
composer config name actency/[project_name]
composer config description [Project description]
composer config repositories.actency/foundation vcs https://gitlab.com/actency/utils/drupal/foundation.git
composer config repositories.drupal/phingdrushtask vcs https://git.drupal.org/project/phingdrushtask.git
composer require actency/foundation
```

## 6. Install dependencies

```
composer install
composer run-script post-install-cmd -d ./vendor/actency/foundation
```

## 7. Configure build properties

Manually edit `/phing/build.properties.project` and configure the settings:

```
website.site.name = [Project name]
website.project.name = [project_name]
website.theme.name = [theme_name]
website.db.name = [db_name]
website.locale = fr
```

`[project_name]` should not contain any special characters, except **dots**.

`[db_name]` should not contain any special characters, except **underscores**.

## 8. Configure build.xml

Manually edit `/build.xml` and configure the project name:

```
<project name="[project_name]" default="help">
```

## 9. Install Drupal

From project root:

```
./vendor/bin/phing install
```

## 10. Generate documentation

To generate documentation, please check the default docker file and uncomment the PHPDoc section (or see an example [here](https://gitlab.com/actency/utils/drupal/init) ), then simply use:

```
./vendor/bin/phing phpdoc
```

*Please note that this process can take up to 30 minutes.*
