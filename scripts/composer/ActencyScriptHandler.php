<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use Composer\Semver\Comparator;
use Symfony\Component\Filesystem\Filesystem;

class ActencyScriptHandler {

  protected static function getDrupalRoot($project_root) {
    return $project_root . '/web';
  }

  public static function createRequiredFiles(Event $event) {
    $fs = new Filesystem();
    $root = getcwd().'/../../..'; // vendor/actency/foundation

    // Create phing folder
    $dir = 'phing';
    if (!$fs->exists($root . '/'. $dir)) {
      $event->getIO()->write('Creating' . $root . '/'. $dir);
      $fs->mkdir($root . '/'. $dir);
      $fs->touch($root . '/'. $dir . '/.gitkeep');
      $event->getIO()->write("Created phing folder");
    }

    $files = [
      [
        'source' => $root . '/vendor/actency/foundation/phing/example.build.properties.local',
        'dest' => $root . '/phing/example.build.properties.local',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/phing/travis.build.properties.local',
        'dest' => $root . '/phing/travis.build.properties.local',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/phing/build.properties.project',
        'dest' => $root . '/phing/build.properties.project',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/example.build.xml',
        'dest' => $root . '/build.xml',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/example.hash1temp.txt',
        'dest' => $root . '/scripts/drupal/hash1temp.txt',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/drupal/example.settings.php',
        'dest' => $root . '/config/drupal/example.settings.php',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/drupal/example.settings.local.php',
        'dest' => $root . '/config/drupal/example.settings.local.php',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/vendor/actency/foundation/drupal/example.settings.travis.php',
        'dest' => $root . '/config/drupal/example.settings.travis.php',
        'mode' => 'copy',
      ],
      [
        'source' => $root . '/web/sites/example.settings.local.php',
        'dest' => $root . '/config/drupal/example.settings.local.php',
        'mode' => 'copy',
      ],
    ];

    foreach ($files as $file) {
      if ($fs->exists($file['source'])) {
        switch ($file['mode']) {
          case 'copy':
          case 'overwrite':
            if ($file['mode'] == 'overwrite' || !$fs->exists($file['dest'])) {
              $fs->copy($file['source'], $file['dest']);
              $event->getIO()->write('Created ' . $file['dest']);
            }
            break;
          case 'link':
            if (!$fs->exists($file['dest'])) {
              if (method_exists($fs, 'hardlink')) {
                $fs->hardlink($file['source'], $file['dest']);
              }
              else {
                $fs->copy($file['source'], $file['dest']);
              }
              $event->getIO()->write('Created ' . $file['dest']);
            }
            break;
          default:
            break;
        }
      }
    }

    // Create phing/tasks folder
    $dir = 'phing/tasks';
    if (!$fs->exists($root . '/'. $dir)) {
      $event->getIO()->write('Creating' . $root . '/'. $dir);
      $fs->mkdir($root . '/'. $dir);
      $fs->touch($root . '/'. $dir . '/.gitkeep');
      $event->getIO()->write("Created phing/tasks folder");
    }

    $tasks = [
      [
        'name' => 'project.xml',
        'mode' => 'copy',
      ],
    ];

    foreach ($tasks as $task) {
      $task['source'] = $root . '/vendor/actency/foundation/phing/tasks/' . $task['name'];
      $task['dest'] = $root . '/phing/tasks/' . $task['name'];
      if ($fs->exists($file['source'])) {
        switch ($task['mode']) {
          case 'copy':
            if (!$fs->exists($task['dest'])) {
              $fs->copy($task['source'], $task['dest']);
              $event->getIO()->write('Created ' . $task['dest']);
            }
            break;
          case 'link':
            if (!$fs->exists($task['dest'])) {
              if (method_exists($fs, 'hardlink')) {
                $fs->hardlink($task['source'], $task['dest']);
              }
              else {
                $fs->copy($task['source'], $task['dest']);
              }
              $event->getIO()->write('Created ' . $task['dest']);
            }
            break;
          default:
            break;
        }
      }
    }
  }

  /**
   * Checks if the installed version of Composer is compatible.
   *
   * Composer 1.0.0 and higher consider a `composer install` without having a
   * lock file present as equal to `composer update`. We do not ship with a lock
   * file to avoid merge conflicts downstream, meaning that if a project is
   * installed with an older version of Composer the scaffolding of Drupal will
   * not be triggered. We check this here instead of in drupal-scaffold to be
   * able to give immediate feedback to the end user, rather than failing the
   * installation after going through the lengthy process of compiling and
   * downloading the Composer dependencies.
   *
   * @see https://github.com/composer/composer/pull/5035
   */
  public static function checkComposerVersion(Event $event) {
    $composer = $event->getComposer();
    $io = $event->getIO();

    $version = $composer::VERSION;

    // The dev-channel of composer uses the git revision as version number,
    // try to the branch alias instead.
    if (preg_match('/^[0-9a-f]{40}$/i', $version)) {
      $version = $composer::BRANCH_ALIAS_VERSION;
    }

    // If Composer is installed through git we have no easy way to determine if
    // it is new enough, just display a warning.
    if ($version === '@package_version@' || $version === '@package_branch_alias_version@') {
      $io->writeError('<warning>You are running a development version of Composer. If you experience problems, please update Composer to the latest stable version.</warning>');
    }
    elseif (Comparator::lessThan($version, '1.0.0')) {
      $io->writeError('<error>Drupal-project requires Composer version 1.0.0 or higher. Please update your Composer before continuing</error>.');
      exit(1);
    }
  }

}
